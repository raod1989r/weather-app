import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';

import WeatherComponent from './weather/WeatherComponent';

function App() {
  useEffect(() => {
    console.log('Updates')
  })

  return (
    <div className="App">
      <header className="App-header">
        Weather in slected city from LA
        <img src={logo} className="App-logo" alt="logo" />
        <WeatherComponent/>
      </header>
    </div>
  );
}

export default App;
