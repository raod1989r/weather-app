import React from 'react';

export function TimeZone(props){
    return (
        <td>
              Timezone: {props.timeZone}
        </td>
    )
}