import React from 'react';
import axios from 'axios';
import {TimeZone} from './TimeZoneComponent';
import {Temperature} from './TemperatureComponent';

class WeatherComponent extends React.Component {
    constructor(props) {
      super(props);  

      this.state = {
        city: null,
        qualifiedCities: [],
        selectedCity: 'N/A', 
        timeZone: 'N/A', 
        temperature: 'N/A', 
        latitude: null,
        longitude: null
      }
    }

    componentDidMount(){
      if (navigator.geolocation) {  
          let currentPosition;    
          function getCurrentLocation (position) {     
            currentPosition = position.coords;        

            this.state = {
              latitude: currentPosition.latitude,
              longitude: currentPosition.longitude
            }

            this.search();
        }

        navigator.geolocation.getCurrentPosition(getCurrentLocation.bind(this));
      }
    }

    getTimeZoneDetails(lat, long){
      const timeZoneURL = `http://api.geonames.org/timezone?lat=${lat}&lng=${long}&username=raod1989r`;


      axios({
        method: 'GET',
        url: timeZoneURL
      })
      .then(response => {
        console.log('???', response)
      })
    }

    getWeatherDetails(lat, long){
      const weatherAPIKey = '79d8e6fa608c4b3299f6054a0da6c2e0';
      const weatherURL = `http://api.openweathermap.org/data/2.5/forecast/daily?lat=${lat}&lon=${long}&cnt=${1}&APPID=${weatherAPIKey}`;


      axios({
        method: 'GET',
        url: weatherURL
      })
      .then(response => {
        console.log(response, '????###')  
      })
    }

    findCoordinatesOfCity(city, cb){
      const latLngURL = `http://api.geonames.org/geoCodeAddress?q=${city}&username=raod1989r`;

      axios({
        method: 'GET',
        url: latLngURL
      })
      .then(response => {
        console.log(response, '????')  
        cb();
      })
    }

    search(isCity){
      let onSuccess = (latitude, longitude) => {
        this.getWeatherDetails(latitude, longitude);
        this.getTimeZoneDetails(latitude, longitude);  
      }

      if(isCity){
        this.findCoordinatesOfCity(this.state.city, onSuccess.bind(this));
      } else {
        onSuccess(this.state.latitude, this.state.longitude);
      }
    }

    setCity(ev){
      let city = ev.currentTarget.value;

      this.setState({
        city: city
      })
    }

    render() {
      const {selectedCity, temperature, timeZone} = this.state;

      return(
        <div>
        <table>
          <tr>
            <td>
              City: {selectedCity}
            </td>
            <Temperature temperature={temperature} />
            <TimeZone timeZone={timeZone} />
          </tr>
        </table>
        <input placeholder={'Enter the city'} onChange={(ev) => this.setCity(ev)}/>
        <button onClick={ev => this.search(true)}>Search</button>
        </div>
      );
    }
  }

  export default WeatherComponent = WeatherComponent;