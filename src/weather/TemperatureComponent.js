import React from 'react';

export function Temperature(props){
    return (
        <td>
            Weather: {props.temperature}
        </td>
    )
}